import gym
import numpy as np

from src.config import (
    ELEC_PRICE,
    ENERGY_PRICES,
    FOSSIL_STARTING_TIME,
    INF_REWARD,
    MEAN_SOLAR_INCIDENT,
    MIN_WIND_SPEED,
    NB_DAYS,
    NB_HOURS_PER_DAY,
    NUCLEAR_STARTING_TIME,
    OPTI_WIND_SPEED,
    POWER_EFFICIENCY,
    PRODUCER_MARGIN,
    logging,
)
from src.smartgrid.data_loader import load_weather_data


def reward_func_producer(
    production: float, consumption: float, price: float
) -> float:
    """Reward function for the producer."""
    if abs(production - consumption) / consumption > 0.05:
        return -INF_REWARD
    return -price


class SmartGridProducer(gym.Env):
    """
    ### Description
    This is the SmartgridProducer class which represent all the producers. It
    is a gym environment.

    ### Observation space

    The observation is a (6,) gym.spaces.Box
    - fossil_starting_state : the number of time since activation for the fossil
    energy plant
    - solar_radiation : the current solar incident energy
    - nuclear_starting_state : the number of time since activation for the
    nuclear plant
    - wind_speed
    - wind angle
    - electric_consumption from the consumer

    ### Action space
    The action is a (4,) gym.spaces.Box
    - the input energy for fossil energy production
    - the input energy for solar energy production
    - the input energy for nuclear energy production
    - the input energy for wind energy production
    """

    def __init__(self, weather_path: str, initial_day: int = None):
        """
        Args:
            weather_path: Path to the weather file.
            nb_days: Number of days of the simulation.
            initial_day: Initial day of the simulation.
        """
        _, solar, wind_angle, wind_speed = load_weather_data(weather_path)
        self.solar_energies = solar.reshape(
            (solar.shape[0] // NB_HOURS_PER_DAY, NB_HOURS_PER_DAY)
        )
        self.wind_speeds = wind_speed.reshape(
            (wind_speed.shape[0] // NB_HOURS_PER_DAY, NB_HOURS_PER_DAY)
        )
        self.wind_angles = wind_angle.reshape(
            (wind_angle.shape[0] // NB_HOURS_PER_DAY, NB_HOURS_PER_DAY)
        )

        self.day, self.hour = 0, 0
        self.initial_day = initial_day
        self.consumer_consumption = 0.0
        self.consumer_production = 0.0
        self.last_production = 0.0
        self.last_unit_price = 0.0
        self.last_production_price = 0.0
        self.last_selling_price = 0.0
        self.last_energies = [0.0, 0.0, 0.0, 0.0]
        self.last_produced_energy = 0
        self.marge = 0.0

        low_obs = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        high_obs = [
            FOSSIL_STARTING_TIME,
            2.0,
            NUCLEAR_STARTING_TIME,
            200.0,
            360.0,
            1000.0,
        ]
        self.observation_space = gym.spaces.Box(
            low=np.array(low_obs), high=np.array(high_obs), dtype=np.float32
        )

        low_act = [0.0, 0.0, 0.0, 0.0]
        high_act = [np.Inf, np.Inf, np.Inf, np.Inf]
        self.action_space = gym.spaces.Box(
            low=np.array(low_act), high=np.array(high_act), dtype=np.float32
        )

        self.fossil_starting_state = 0
        self.nuclear_starting_state = 0

    def get_state_action_space(self) -> (gym.spaces.Box, gym.spaces.Box):
        """Returns the observation and action space."""
        return self.observation_space, self.action_space

    def get_total_margin(self) -> float:
        """
        Returns:
            The total margin of the producer.
        """
        return self.marge

    def get_last_production_price(self) -> float:
        """
        Returns:
            The total price for the electricity production of the last hour.
        """
        return self.last_production_price

    def get_last_selling_price(self) -> float:
        """
        Returns:
            The total price for the electricity selling of the last hour.
        """
        return self.last_selling_price

    def get_date(self) -> (int, int):
        """
        Returns:
            The current day and the current hour in the simulation.
        """
        return self.day, self.hour

    def update_next_time(self) -> None:
        """Add one hour to the current time"""
        self.day += (self.hour + 1) // NB_HOURS_PER_DAY
        self.hour = (self.hour + 1) % NB_HOURS_PER_DAY

    def update_consumer_consumption(self, consumption: float) -> None:
        """Update the consumer consumption."""
        self.consumer_consumption = consumption

    def get_generation(self, energy_type: str, power: float) -> float:
        """
        Args:
            type: Energy type.
            power: Input power in kWh.

        Returns:
            The generated power for the specified energy type and the energy
            input in kWh.
        """

        if energy_type == "fossil":
            return (
                power
                * POWER_EFFICIENCY["fossil"]
                * (self.fossil_starting_state == FOSSIL_STARTING_TIME)
            )
        if energy_type == "solar":
            return (
                power
                * POWER_EFFICIENCY["solar"]
                * (
                    self.solar_energies[self.day][self.hour]
                    / MEAN_SOLAR_INCIDENT
                )
            )
        if energy_type == "nuclear":
            return (
                power
                * POWER_EFFICIENCY["nuclear"]
                * (self.nuclear_starting_state == NUCLEAR_STARTING_TIME)
            )
        if energy_type == "wind":
            return (
                power
                * POWER_EFFICIENCY["wind"]
                * (
                    (self.wind_speeds[self.day][self.hour] / OPTI_WIND_SPEED)
                    ** 3
                )
                * (self.wind_speeds[self.day][self.hour] >= MIN_WIND_SPEED)
            )

    def step(self, action: list) -> (gym.spaces.Box, float, bool, dict):
        """Gym environment step function.

        Args:
            actions: Actions given to the producer.

        Returns:
            states: New state for the producer after executing the actions.
            reward: Reward to give to the agent.
            finished: True if the simulation is over.
            info: A dict with info on the simulation.
        """

        fossil_power, solar_power, nuclear_power, wind_power = action

        fossil_energy = (
            fossil_power
            * POWER_EFFICIENCY["fossil"]
            * (self.fossil_starting_state == FOSSIL_STARTING_TIME)
        )
        solar_energy = (
            solar_power
            * POWER_EFFICIENCY["solar"]
            * self.solar_energies[self.day][self.hour]
            / MEAN_SOLAR_INCIDENT
        )
        nuclear_energy = (
            nuclear_power
            * POWER_EFFICIENCY["nuclear"]
            * (self.nuclear_starting_state == NUCLEAR_STARTING_TIME)
        )
        wind_energy = (
            wind_power
            * POWER_EFFICIENCY["wind"]
            * ((self.wind_speeds[self.day][self.hour] / OPTI_WIND_SPEED) ** 3)
            * (self.wind_speeds[self.day][self.hour] >= MIN_WIND_SPEED)
        )

        produced_energy = (
            fossil_energy + solar_energy + nuclear_energy + wind_energy
        )

        self.fossil_starting_state = min(
            FOSSIL_STARTING_TIME,
            max(
                0,
                self.fossil_starting_state
                + (fossil_power > 0)
                - (fossil_power == 0),
            ),
        )

        self.nuclear_starting_state = min(
            NUCLEAR_STARTING_TIME,
            max(
                0,
                self.nuclear_starting_state
                + (nuclear_power > 0)
                - (nuclear_power == 0),
            ),
        )

        self.update_next_time()

        states = np.array(
            [
                FOSSIL_STARTING_TIME,
                self.solar_energies[self.day][self.hour],
                NUCLEAR_STARTING_TIME,
                self.wind_speeds[self.day][self.hour],
                self.wind_angles[self.day][self.hour],
                self.consumer_consumption,
            ]
        )
        self.last_production = produced_energy

        price = (
            fossil_energy * ENERGY_PRICES["fossil"]
            + solar_energy * ENERGY_PRICES["solar"]
            + nuclear_energy * ENERGY_PRICES["nuclear"]
            + wind_energy * ENERGY_PRICES["wind"]
        )
        self.last_energies = [
            fossil_energy,
            solar_energy,
            nuclear_energy,
            wind_energy,
        ]
        self.last_produced_energy = produced_energy

        if self.consumer_consumption > 0:
            self.last_unit_price = price / self.consumer_consumption
        else:
            self.last_unit_price = ELEC_PRICE
        self.last_production_price = (
            self.last_unit_price * self.consumer_consumption
        )
        self.last_selling_price = (
            self.last_unit_price
            * self.consumer_consumption
            * (1 + PRODUCER_MARGIN)
        )
        self.marge += self.last_selling_price - self.last_production_price

        reward = reward_func_producer(
            self.last_production,
            self.consumer_consumption,
            self.last_production_price,
        )
        finished = NB_DAYS == self.day

        return states, reward, finished, {}

    def describe(self):
        """Logging function for the class."""
        logging.info(
            "Producer side:"
            f"nuclear starting state: {self.nuclear_starting_state}"
            f"fossil starting state: {self.fossil_starting_state}"
            f"fossil energy: {self.last_energies[0]} kWh"
            f"solar energy: {self.last_energies[1]} kWh"
            f"nuclear energy: {self.last_energies[2]} kWh"
            f"wind energy: {self.last_energies[3]} kWh"
            f"total generated: {self.last_produced_energy} kWh "
            f"to match demand ({self.consumer_consumption} kWh)"
        )

    def render(self, mode="human"):
        """Render the environment."""
        print(self.observation_space, self.action_space)

    def reset(self):
        """Gym reset function."""
        self.hour = 0
        self.day = self.initial_day
        self.consumer_consumption = 0.0
        self.consumer_production = 0.0
        self.last_production = 0.0
        self.last_price = 0.0
        self.last_unit_price = 0.0
        self.fossil_starting_state = 0
        self.nuclear_starting_state = 0
