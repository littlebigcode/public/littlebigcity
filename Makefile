.PHONY: format flake8 pylint format-check

VENVS:=$(shell find . -maxdepth 1 -type d | grep venv)
VENV_LIST:=$(shell echo $(addsuffix "_", ${VENVS}) | sed "s/_/,/g" | sed "s/\ //g")
ISORT_VENV:=$(shell echo $(addprefix "_", ${VENVS}) | sed "s/_.\// --virtual-env /g")
SRC_PACKAGES:=$(shell find . -maxdepth 2 -type f -name "__init__.py" -exec dirname \{\} \; | grep -v "tests")
ALL_PACKAGES:=$(shell find . -maxdepth 4 -type f -name "__init__.py" -exec dirname \{\} \;)

format:
	isort --profile black $(ISORT_VENV) -l 80 $(ALL_PACKAGES)
	black -l 80 .

flake8:
	flake8 --exclude $(VENV_LIST)

pylint:
	pylint $(SRC_PACKAGES) --rcfile ./.pylintrc

format-check:
	isort --profile black --check $(ISORT_VENV) -l 80 $(ALL_PACKAGES)
	black -l 80 --check .
