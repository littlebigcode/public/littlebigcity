from src.config import T_MAX, T_MIN
from src.smartgrid.smartgrid_consumer import SmartGridConsumer


class BruteforceAgent:
    """A bruteforce agent which simulate all the consumers in the smartgrid.

    It uses the maximal amount of electricity to control the temperature of
    the building (the electric heater and the heat pump are given the maximal
    amount of input energy when the outside is cold). The battery is charged at
    night time (9pm to 8am) and discharged during the day.
    Next agents might use RL to simulate a consumer.
    """

    def get_action(self, state: list, smartgrid: SmartGridConsumer) -> list:
        """
        Returns:
             The actions for the consumer part of the smartgrid and the
        temperatures of each building.
        """

        actions, temp = [], []
        for building in smartgrid.buildings:
            # battery action (charging at night, discharging otherwise)
            if building.electricity_storage is not None:
                if 0 <= smartgrid.hour <= 8 or 21 <= smartgrid.hour <= 24:
                    actions.append(1.0)
                else:
                    actions.append(-1.0)

            # heater action (maximum activity if the building need heat)
            if building.electric_heater is not None:
                if building.get_temperature() > T_MIN:
                    actions.append(0.0)
                else:
                    actions.append(building.electric_heater.max_input_energy)

            # heat pump action (maximum heating or cooling whether the house is
            # cold or hot)
            if building.hp_device is not None:
                if building.get_temperature() < T_MIN:
                    actions.extend((0.0, building.hp_device.max_consumption))
                elif building.get_temperature() > T_MAX:
                    actions.extend((building.hp_device.max_consumption, 0.0))
                else:
                    actions.extend((0.0, 0.0))

            temp.append(building.get_temperature())
        return actions
