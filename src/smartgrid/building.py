"""
SmartGrid project

Building class
"""

import gym
import numpy as np

from src.config import C_VOL_AIR, NB_HOURS_PER_DAY, NB_SEC_PER_HOUR, logging


class Building:
    """Class representing a building (one consumer).

    Args:
        building_id: Id for the building as an int.
        building_volume: Volume of the building (in m^3).
        initial_temperature: Initial temperature of the building (in °C).
        thermal_resistance: Thermal resistance of the building (SI).
        solar_generator: SolarPanel of the building.
        electricity_storage: BatteryStorage of the building.
        electric_heater: ElectricHeater of the building.
        hp_device: HeatPump of the building.
        consuming_curve: Electric consumption of the appliances for 24h (kWh).

    Attributes:
        building_id: Id for the building as an int.
        building_volume: Volume of the building (in m^3).
        initial_temperature: Initial temperature of the building (in °C).
        thermal_resistance: Thermal resistance of the building (SI).
        solar_generator: SolarPanel of the building.
        electricity_storage: BatteryStorage of the building.
        electric_heater: ElectricHeater of the building.
        hp_device: HeatPump of the building.
        electric_consuming_curve: Hourly appliances consumption for 24h (kWh).
        last_temperature: Last temperature inside the building.
        last_outside_temp: Last temperature outside the building.
        last_appliances_consumption: Last hourly appliances consumption (kWh).
        last_consumption: Last electricity consumption of the building.
        generation_history: History of the electricity generated.
        observation_space: Gym environment observation space.
        action_space: Gym environment action space.
    """

    def __init__(
        self,
        building_id: int = None,
        building_volume: float = None,
        initial_temperature: float = None,
        thermal_resistance: float = None,
        solar_generator=None,
        electricity_storage=None,
        electric_heater=None,
        hp_device=None,
        consuming_curve: list = None,
    ):
        self.building_id = building_id
        # parameters
        self.building_volume = building_volume
        self.initial_temperature = initial_temperature
        self.thermal_resistance = thermal_resistance
        self.electric_consuming_curve = consuming_curve
        # devices
        self.solar_generator = solar_generator
        self.electricity_storage = electricity_storage
        self.electric_heater = electric_heater
        self.hp_device = hp_device

        self.initial_temperature = initial_temperature
        self.last_temperature = initial_temperature
        self.last_outside_temp = None
        self.last_appliances_consumption = 0
        self.last_consumption = 0
        self.generation_history = []
        self.observation_space, self.action_space = None, None

    def get_temperature(self) -> float:
        """
        Returns:
             The last temperature of the building.
        """
        return self.last_temperature

    def set_action_space(self, min_action, max_action) -> None:
        """Set the action space of the environment."""
        self.action_space = gym.spaces.Box(
            low=min_action, high=max_action, dtype=np.float32
        )

    def set_observation_space(self, min_obs, max_obs) -> None:
        """Set the observation space of the environment."""
        self.observation_space = gym.spaces.Box(
            low=min_obs, high=max_obs, dtype=np.float32
        )

    def update_temperature(self, t_outside: float) -> None:
        """Calculate the new temperature of the building.

        Update the temperature of the building given all its components
        and the heat exchange with the outside.

        Args:
            t_outside: Outside temperature.
        """
        q_iso = (
            NB_SEC_PER_HOUR
            * (t_outside - self.last_temperature)
            / self.thermal_resistance
            / 1000.0
        )
        thermal_energy = 0

        if self.electric_heater is not None:
            thermal_energy += self.electric_heater.get_thermal_energy()
        if self.hp_device is not None:
            thermal_energy += self.hp_device.get_thermal_energy()

        delta_t = (
            1000 * (thermal_energy + q_iso) / (C_VOL_AIR * self.building_volume)
        )

        self.last_temperature += delta_t
        self.last_outside_temp = t_outside

    def get_last_battery(self) -> float:
        """
        Returns:
            The last state of charge of the battery.
        """
        if self.electricity_storage is not None:
            return self.electricity_storage.get_soc()
        return 0.0

    def get_last_daily_battery(self) -> list:
        """
        Returns:
            A list with the state of charge of the battery for the last 24h.
        """
        if (
            self.electricity_storage is not None
            or len(self.electricity_storage.soc_history) < NB_HOURS_PER_DAY
        ):
            return self.electricity_storage.soc_history[-NB_HOURS_PER_DAY:]
        return [0] * NB_HOURS_PER_DAY

    def get_energy_appliances(self, time: int) -> float:
        """
        Returns:
            The appliances' electricity consumption for the specified time.
        """
        self.last_appliances_consumption = self.electric_consuming_curve[time]
        return self.electric_consuming_curve[time]

    def get_last_consumption(self) -> float:
        """
        Returns:
             The last electricity consumption by the building.
        """
        return self.last_consumption

    def get_energy_consumption(self, time: int) -> float:
        """
        Returns:
             The global energy consumption of the building for the last hour.
        """
        total = self.get_energy_appliances(time)
        generated = 0

        for element in [
            self.solar_generator,
            self.electricity_storage,
            self.electric_heater,
            self.hp_device,
        ]:
            if element is not None:
                total += element.get_energy_consumption()
                if element.get_energy_consumption() < 0:
                    generated -= element.get_energy_consumption()

        self.last_consumption = total
        self.generation_history.append(generated)

        return total

    def reset(self) -> None:
        """Gym environment reset function."""

        # reset parameters if needed
        self.last_temperature = self.initial_temperature
        self.last_outside_temp = None
        self.last_appliances_consumption = 0
        self.last_consumption = 0
        self.generation_history = []

        # reset devices
        for element in [
            self.solar_generator,
            self.electricity_storage,
            self.electric_heater,
            self.hp_device,
        ]:
            if element is not None:
                element.reset()

    def get_generated_energy(self) -> list:
        """
        Returns:
            The electricity generated by the building at each timestep.
        """
        return self.generation_history

    def get_last_generation(self) -> float:
        """
        Returns:
            The electricity generated by the building at the last timestep.
        """
        return self.generation_history[-1]

    def describe(self) -> None:
        """Logging function for the building class."""
        logging.info(
            f"Building {self.building_id}:\n"
            f"inside T={self.last_temperature} °C\n"
            f"outside T={self.last_outside_temp} °C\n"
            "Consommation éléctrique des appareils="
            f"{self.last_appliances_consumption} kWh\n"
            f"TOTAL {self.last_consumption} kWh\n"
        )

        for element in [
            self.solar_generator,
            self.electricity_storage,
            self.electric_heater,
            self.hp_device,
        ]:
            if element is not None:
                element.describe()
