import gym
import numpy as np
import torch
import torch.nn.functional as F
from torch import nn
from torch.autograd import Variable

from src.config import ACTOR_DDPG_PATH, CRITIC_DDPG_PATH
from src.smartgrid.smartgrid_consumer import SmartGridConsumer

EPS = 1e-3


def fanin_init(size: np.array, fanin: np.array = None) -> torch.Tensor:
    """Initialize the weights with a fan-in.

    Args:
        size: The size of the weight tensor to init.
        fanin: The fan-in to use. If None, calculate the fan-in from the size.
    """
    fanin = fanin or size[0]
    value = 1.0 / np.sqrt(fanin)
    return torch.Tensor(size).uniform_(-value, value)


class Critic(nn.Module):
    """Critic part of the Actor-Critic algorithm.

    Args:
        observation_space: Gym environment observation space.
        action_space: Gym environment action space.

    Attributes:
        state_dim: Dimension of the observation space.
        action_dim: Dimension of the action space.
        layer1: First hidden layer of the critic.
        layer2: Second hidden layer of the critic.
        layer3: Output layer of the critic.
    """

    def __init__(
        self, observation_space: gym.spaces.Box, action_space: gym.spaces.Box
    ):
        super().__init__()
        self.state_dim = observation_space.shape[0]
        self.action_dim = action_space.shape[0]

        self.layer1 = nn.Linear(self.state_dim, 400)
        self.layer1.weight.data = fanin_init(self.layer1.weight.data.size())
        self.layer2 = nn.Linear(400 + self.action_dim, 300)
        self.layer2.weight.data = fanin_init(self.layer2.weight.data.size())
        self.layer3 = nn.Linear(300, 1)
        self.layer3.weight.data.uniform_(-EPS, EPS)

    def forward(self, state: np.array, action: np.array) -> torch.Tensor:
        """Forward pass of the critic.

        Args:
            state: Current state of the environment.
            action: Current action of the environment.

        Returns:
            Value Q(s,a) from critic network.
        """

        x = F.relu(self.layer1(state))
        x = F.relu(self.layer2(torch.cat((x, action), dim=1)))
        x = self.layer3(x)

        return x


class Actor(nn.Module):
    """Actor part of the Actor-Critic algorithm.

    Args:
        observation_space: Gym environment observation space.
        action_space: Gym environment action space.

    Attributes:
        state_dim: Dimension of the observation space.
        action_dim: Dimension of the action space.
        layer1: First hidden layer of the actor.
        layer2: Second hidden layer of the actor.
        layer3: Output layer.
    """

    def __init__(
        self, observation_space: gym.spaces.Box, action_space: gym.spaces.Box
    ):
        super().__init__()

        self.observation_space = observation_space
        self.action_space = action_space

        self.state_dim = observation_space.shape[0]
        self.action_dim = action_space.shape[0]

        self.layer1 = nn.Linear(self.state_dim, 400)
        self.layer1.weight.data = fanin_init(self.layer1.weight.data.size())
        self.layer2 = nn.Linear(400, 300)
        self.layer2.weight.data = fanin_init(self.layer2.weight.data.size())
        self.layer3 = nn.Linear(300, self.action_dim)
        self.layer3.weight.data.uniform_(-EPS, EPS)

    def forward(self, state: np.array) -> torch.Tensor:
        """Forward pass of the actor.

        Args:
            state: Current state of the environment.

        Returns:
            Policy pi(s) from actor network scaled to the environment
            action space.
        """
        x = F.relu(self.layer1(state))
        x = F.relu(self.layer2(x))
        action = torch.tanh(self.layer3(x))
        action = (action + 1) / 2
        action = action * torch.from_numpy(
            self.action_space.high - self.action_space.low
        ) + torch.from_numpy(self.action_space.low)
        return action


class ExperienceReplay:
    """Experience replay buffer.

    Args:
        state_dim: Dimension of the observation space.
        action_dim: Dimension of the action space.
        buffer_size: Size of the buffer.

    Attributes:
        state_mem: Memory of the states.
        action_mem: Memory of the actions.
        reward_mem: Memory of the rewards.
        next_state_mem: Memory of the next states.
        done_mem: Memory of the done flags.
        pointer: Pointer to the current position in the memory.
    """

    def __init__(
        self, state_dim: int, action_dim: int, buffer_size: int = 10000
    ):
        self.buffer_size = buffer_size
        self.state_dim = state_dim
        self.action_dim = action_dim
        self.state_mem = np.zeros((self.buffer_size, state_dim))
        self.action_mem = np.zeros((self.buffer_size, action_dim))
        self.reward_mem = np.zeros(self.buffer_size)
        self.done_mem = np.zeros(self.buffer_size)
        self.next_state_mem = np.zeros((self.buffer_size, state_dim))
        self.pointer = 0

    def add_exp(
        self,
        state: np.array,
        action: np.array,
        reward: int,
        next_state: np.array,
        done: bool,
    ):
        """Add an experience to the memory.

        Args:
            state: Current state of the environment.
            action: Current action of the environment.
            reward: Reward received from the environment.
            next_state: Next state of the environment.
            done: Done flag of the environment."""
        idx = self.pointer % self.buffer_size
        self.state_mem[idx] = state
        self.action_mem[idx] = action
        self.reward_mem[idx] = reward
        self.next_state_mem[idx] = next_state
        self.done_mem[idx] = done
        self.pointer += 1

    def sample(
        self, batch_size: int = 128
    ) -> (np.array, np.array, np.array, np.array, np.array):
        """Sample a batch of experiences from the memory.

        Args:
            batch_size: Size of the batch.

        Returns:
            States, actions, rewards, next states , dones of the environment.
        """
        max_mem = min(self.pointer, self.buffer_size)
        batch = np.random.choice(max_mem, batch_size, replace=False)
        states = self.state_mem[batch]
        actions = self.action_mem[batch]
        rewards = self.reward_mem[batch]
        next_states = self.next_state_mem[batch]
        dones = self.done_mem[batch]
        return states, actions, rewards, next_states, dones


class OrnsteinUhlenbeckActionNoise:
    """Ornstein-Uhlenbeck action noise for policy exploration.

    Args:
        action_dim: Dimension of the action space.
        eta: Scale of the noise.
        theta: Mean of the noise.
        sigma: Standard deviation of the noise.
        values: List of values to sample from.
    """

    def __init__(self, action_dim, eta=0, theta=0.15, sigma=0.2):
        self.action_dim = action_dim
        self.eta = eta
        self.theta = theta
        self.sigma = sigma
        self.values = np.ones(self.action_dim) * self.eta

    def reset(self):
        """Reset the noise to its initial state."""
        self.values = np.ones(self.action_dim) * self.eta

    def sample(self):
        """Sample a noise from the Ornstein-Uhlenbeck process."""
        deriv = self.theta * (self.eta - self.values)
        deriv = deriv + self.sigma * np.random.randn(len(self.values))
        self.values = self.values + deriv
        return self.values


class Agent:
    """Agent implementing the Actor-Critic algorithm.

    Args:
        env: Gym environment.
        critic_learning_rate: Learning rate of the critic.
        actor_learning_rate: Learning rate of the actor.
        gamma: Discount factor.
        tau: Soft update factor.
        batch_size: Size of the batch for the experience replay.
        buffer_size: Size of the experience replay buffer.

    Attributes:
        state_dim: Dimension of the observation space.
        action_dim: Dimension of the action space.
        noise: Ornstein-Uhlenbeck action noise.
        replay: Experience replay buffer.
        actor: Actor network.
        target_actor: Target actor network.
        actor_optimizer: Optimizer for the actor network.
        critic: Critic network.
        target_critic: Target critic network.
        critic_optimizer: Optimizer for the critic network.
    """

    def __init__(
        self,
        env: gym.Env,
        critic_learning_rate: float = 0.0001,
        actor_learning_rate: float = 0.0001,
        gamma: float = 0.99,
        tau: float = 0.001,
        batch_size: int = 64,
        buffer_size: int = 1000000,
    ):
        self.state_dim = env.observation_space.shape[0]
        self.action_dim = env.action_space.shape[0]
        self.critic_learning_rate = critic_learning_rate
        self.actor_learning_rate = actor_learning_rate
        self.tau = tau
        self.gamma = gamma
        self.batch_size = batch_size
        self.buffer_size = buffer_size
        self.iter = 0

        self.noise = OrnsteinUhlenbeckActionNoise(self.action_dim)

        self.env = env
        self.replay = ExperienceReplay(
            self.state_dim, self.action_dim, self.buffer_size
        )

        self.actor = Actor(env.observation_space, env.action_space)
        self.target_actor = Actor(env.observation_space, env.action_space)
        self.actor_optimizer = torch.optim.Adam(
            self.actor.parameters(), self.actor_learning_rate
        )

        self.critic = Critic(env.observation_space, env.action_space)
        self.target_critic = Critic(env.observation_space, env.action_space)
        self.critic_optimizer = torch.optim.Adam(
            self.critic.parameters(),
            self.critic_learning_rate,
        )
        self.hard_target_copy()

    def soft_target_copy(self):
        """Soft target copy.

        Update the target networks by copying the weights of the actor and
        critic networks with the soft update factor tau."""
        for target_param, param in zip(
            self.target_actor.parameters(), self.actor.parameters()
        ):
            target_param.data.copy_(
                target_param.data * (1.0 - self.tau) + param.data * self.tau
            )

        for target_param, param in zip(
            self.target_critic.parameters(), self.critic.parameters()
        ):
            target_param.data.copy_(
                target_param.data * (1.0 - self.tau) + param.data * self.tau
            )

    def hard_target_copy(self):
        """Hard target copy.

        Update the target networks by copying the weights of the actor and
        critic networks"""
        for target_param, param in zip(
            self.target_actor.parameters(), self.actor.parameters()
        ):
            target_param.data.copy_(param.data)

        for target_param, param in zip(
            self.target_critic.parameters(), self.critic.parameters()
        ):
            target_param.data.copy_(param.data)

    def get_action(
        self, state: np.array, smartgrid: SmartGridConsumer
    ) -> np.array:
        """Get best action from the actor network.

        Args:
            state: Current state of the environment.
        Returns:
            action: Action to be taken."""

        state = Variable(torch.from_numpy(state.astype(np.float32)))
        action = self.target_actor.forward(state).detach()
        return action.data.numpy()

    def get_random_action(self) -> np.array:
        """Get random action.

        Returns:
            action: Action to be taken."""

        action = self.env.action_space.sample()

        return action

    def get_noisy_exploration_action(self, state: np.array) -> np.array:
        """Get action from the actor network with OrnsteinUhlenbeck noise.

        Args:
            state: Current state of the environment.
        Returns:
            action: Action to be taken."""

        state = Variable(torch.from_numpy(state.astype(np.float32)))
        action = self.actor.forward(state).detach()
        action = action.data.numpy() + (
            self.noise.sample()
            * (self.env.action_space.high - self.env.action_space.low)
        )
        action = np.minimum(action, self.env.action_space.high)
        action = np.maximum(action, self.env.action_space.low)
        return action

    def optimize(self):
        """Optimize the actor and critic networks."""

        states, actions, rewards, next_states, dones = self.replay.sample(
            min(self.batch_size, self.replay.pointer)
        )
        states = Variable(torch.from_numpy(states.astype(np.float32)))
        actions = Variable(torch.from_numpy(actions.astype(np.float32)))
        rewards = Variable(torch.from_numpy(rewards.astype(np.float32)))
        next_states = Variable(torch.from_numpy(next_states.astype(np.float32)))

        # critic optimization
        # loss need to be minimized
        new_actions = self.target_actor.forward(next_states).detach()
        next_val = torch.squeeze(
            self.target_critic.forward(next_states, new_actions).detach()
        )

        y_expected = rewards + self.gamma * next_val * (1 - dones)
        y_predicted = torch.squeeze(self.critic.forward(states, actions))

        loss_critic = F.smooth_l1_loss(y_predicted, y_expected)
        self.critic_optimizer.zero_grad()
        loss_critic.backward()
        self.critic_optimizer.step()

        # actor optimization
        # loss need to be maximized
        pred_actions = self.actor.forward(states)
        loss_actor = -1 * torch.sum(self.critic.forward(states, pred_actions))
        self.actor_optimizer.zero_grad()
        loss_actor.backward()
        self.actor_optimizer.step()

        self.soft_target_copy()
        self.iter += 1

    def save(self):
        """Save the actor and critic networks."""
        torch.save(self.actor.state_dict(), ACTOR_DDPG_PATH)
        torch.save(self.critic.state_dict(), CRITIC_DDPG_PATH)

    def load(self):
        """Load the actor and critic networks."""

        self.actor.load_state_dict(torch.load(ACTOR_DDPG_PATH))
        self.critic.load_state_dict(torch.load(CRITIC_DDPG_PATH))
        self.hard_target_copy()
