from src.config import logging


class ElectricHeater:
    """Class for the electric heater of the building.

    Args:
        efficiency_coeff: Efficiency of the electric heater (as a %).
        max_input_energy: Maximum electric energy that can be given in kWh.

    Attributes:
        efficiency_coeff: Efficiency of the electric heater (as a %).
        max_input_energy: Maximum electric energy that can be given in kWh.
        released_energy: Current thermal energy released in kWh.
        energy_history: History of the released_energy at each timestep.
        last_consumption: Last consumption of the electric heater in kWh.
    """

    def __init__(
        self, efficiency_coeff: float = None, max_input_energy: float = None
    ):

        self.efficiency_coeff = efficiency_coeff
        self.max_input_energy = max_input_energy

        self.released_energy = 0
        self.energy_history = []
        self.last_consumption = 0

    def set_thermal_energy(self, electric_energy: float) -> None:
        """Update the state of the electric heater.

        Args:
            electric_energy: Input energy for the electric heater.
        """
        if self.max_input_energy is not None:
            self.released_energy = self.efficiency_coeff * min(
                electric_energy, self.max_input_energy
            )
        else:
            self.released_energy = self.efficiency_coeff * electric_energy

        self.last_consumption = electric_energy
        self.energy_history.append(self.released_energy)

    def get_thermal_energy(self) -> float:
        """
        Returns:
            The last thermal energy released by the heater.
        """
        return self.released_energy

    def get_energy_consumption(self) -> float:
        """
        Returns:
            The last consumption of the heater.
        """
        return self.last_consumption

    def describe(self) -> None:
        """Logging function for the electric heater."""
        logging.info(
            "Electric heater:\n"
            f"Energy released= {self.released_energy}kWh\n"
            f"Consumption= {self.last_consumption}kWh\n"
        )

    def reset(self) -> None:
        """Reset function for the electric heater."""
        self.released_energy = None
        self.energy_history = []
