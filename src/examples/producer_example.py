import numpy as np

from src.agents.producer_agents.mixed_agent import MixedAgent
from src.config import ENERGY_TYPES, NB_DAYS, NB_HOURS_PER_DAY, WEATHER_PATH
from src.smartgrid.smartgrid_producer import SmartGridProducer


def main():
    """Example script to simulate the smartgrid on the producer side without
    a consumer.
    """

    smartgrid = SmartGridProducer(WEATHER_PATH, NB_DAYS)

    agent = MixedAgent()

    smartgrid.consumer_consumption = 100

    prods = [[] for i in range(len(ENERGY_TYPES))]

    for _ in range(NB_DAYS):
        for _ in range(NB_HOURS_PER_DAY):

            actions, gene = agent.get_actions(smartgrid)
            for i in range(len(ENERGY_TYPES)):
                prods[i].append(gene[i])

            _, _, _, _ = smartgrid.step(np.array(actions))

    prods = np.array(prods)


if __name__ == "__main__":
    main()
