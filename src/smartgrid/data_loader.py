import json

import gym
import numpy as np
import pandas as pd

from src.config import G_ISO
from src.smartgrid.building import Building
from src.smartgrid.components.battery_storage import BatteryStorage
from src.smartgrid.components.electric_heater import ElectricHeater
from src.smartgrid.components.heat_pump import HeatPump
from src.smartgrid.components.solar_panel import SolarPanel


def load_weather_data(
    weather_path: str,
) -> (np.array, np.array, np.array, np.array):
    """Loader for the weather data.

    Args:
        WEATHER_PATH:
            Path for the weather data file, this document should be a csv with
            the following columns:
            -Temperature in °C
            -GHI (Global Solar Irradiation) in kWh/m^2
            -Wind Direction (as an angle in °)
            -Wind Speed (in km/h)

    Returns:
        One np.array per above column.

    """

    df_weather = pd.read_csv(weather_path, delimiter=",", skiprows=2)

    temperatures, solar_radiations = (
        df_weather["Temperature"].values,
        df_weather["GHI"].values / 1000,
    )
    wind_direction, wind_speed = (
        df_weather["Wind Direction"].values,
        df_weather["Wind Speed"].values,
    )

    return temperatures, solar_radiations, wind_direction, wind_speed


def load_building(
    building_path: str,
) -> (list, gym.spaces.Box, gym.spaces.Box, int):
    """Loader for the building data.

    Args:
        building_path:
            Path for the building file. This file should be a json file with
            one key per building. See buildings.json to see an example

    Returns:
        - A list of Building objects
        - The observation space
        - The action space
        - The number of buildings
    """
    with open(building_path, encoding="utf-8") as json_file:
        json_buildings = json.load(json_file)
        json_file.close()

    buildings, nb_buildings, s_min_sg, s_max_sg, a_min_sg, a_max_sg = (
        [],
        0,
        [],
        [],
        [],
        [],
    )

    for building_id, building_data in json_buildings.items():

        s_min, s_max, a_min, a_max = [], [], [], []

        # temperature outside the building
        s_min.append(-50.0)
        s_max.append(50.0)

        # temperature inside the building
        s_min.append(0.0)
        s_max.append(30.0)

        if "solar_panel" in building_data:
            solar_panel = SolarPanel(
                building_data["solar_panel"]["solar_efficiency"],
                building_data["solar_panel"]["solar_area"],
            )
            # sunshine
            s_min.append(0.0)
            s_max.append(1.0)
        else:
            solar_panel = None

        if "battery_storage" in building_data:
            battery_storage = BatteryStorage(
                building_data["battery_storage"]["initial_capacity"],
                building_data["battery_storage"]["irreversible_loss"],
                building_data["battery_storage"]["charging_speed"],
            )
            # state of charge of the battery

            s_min.append(0.0)
            s_max.append(1.0)
            # charge and discharge
            a_min.append(-1.0)
            a_max.append(1.0)
        else:
            battery_storage = None

        if "electric_heater" in building_data:
            electric_heater = ElectricHeater(
                building_data["electric_heater"]["efficiency_coeff"],
                building_data["electric_heater"]["max_input_energy"],
            )
            # energy given to the electric heater
            a_min.append(0.0)
            a_max.append(electric_heater.max_input_energy)

        else:
            electric_heater = None

        if "heat_pump" in building_data:
            heat_pump = HeatPump(
                building_data["heat_pump"]["max_consumption"],
                building_data["heat_pump"]["efficiency_coeff"],
                building_data["heat_pump"]["t_hot"],
                building_data["heat_pump"]["t_cold"],
            )
            # energy given to the heat pump (for cooling & heating)
            a_min.append(0.0)
            a_max.append(heat_pump.max_consumption)

            a_min.append(0.0)
            a_max.append(heat_pump.max_consumption)
        else:
            heat_pump = None

        resistance = (
            1000.0
            / building_data["building_volume"]
            / G_ISO[building_data["isolation"]]
        )

        building = Building(
            building_id,
            building_data["building_volume"],
            building_data["initial_temperature"],
            resistance,
            solar_panel,
            battery_storage,
            electric_heater,
            heat_pump,
            building_data["electric_consuming_curve"],
        )
        nb_buildings, a_max_sg, a_min_sg, s_max_sg, s_min_sg = (
            nb_buildings + 1,
            a_max_sg + a_max,
            a_min_sg + a_min,
            s_max_sg + s_max,
            s_min_sg + s_min,
        )

        building.set_action_space(np.array(a_min), np.array(a_max))
        building.set_observation_space(np.array(s_min), np.array(s_max))

        buildings.append(building)

    observation_space = gym.spaces.Box(
        low=np.float32(np.array(s_min_sg)), high=np.float32(np.array(s_max_sg))
    )
    action_space = gym.spaces.Box(
        low=np.float32(np.array(a_min_sg)), high=np.float32(np.array(a_max_sg))
    )

    return buildings, observation_space, action_space, nb_buildings
