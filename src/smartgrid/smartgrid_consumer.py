from typing import Optional

import gym
import numpy as np

from src.config import INF_REWARD, NB_HOURS_PER_DAY, T_MAX, T_MIN
from src.smartgrid.data_loader import load_building, load_weather_data


def calculate_consuming_reward(
    electric_consumption: float, temperature: float
) -> float:
    """Reward function for one consumer.

    Args:
        electric_consumption:
            Consumption for one building in the smartgrid in the last hour.
        temperature:
            Temperature for this building in the smartgrid in the last_hour.

    Returns:
         The associated reward for this building.
    """
    if T_MIN <= temperature <= T_MAX:
        return 100 - electric_consumption
    return -INF_REWARD * max(temperature - T_MAX, T_MIN - temperature)


def reward_func_consumer(
    electric_consumptions: list, temperatures: list
) -> list:
    """Reward function for all the consumers.

    Args:
        electric_consumptions:
            Consumptions for all the building in the smartgrid last hour.
        temperatures:
            Temperatures for all the building in the smartgrid last_hour.

    Returns:
         The associated reward for each building.
    """

    return [
        calculate_consuming_reward(electric_consumptions[i], temperatures[i])
        for i in range(len(electric_consumptions))
    ]


class SmartGridConsumer(gym.Env):
    """
    ### Description
    The consumer side smart grid class.

    ### Observation space
    This class stores the different consumer buildings of the smartgrid
    and this is a gym environment.
    The states of this environment are for each building
    1. the outside temperature
    2. the inside temperature
    3. the sunshine (if there is a solar panel)
    4. the state of charge of the battery (is  there is a battery)
    The state of the smartgrid is the concatenation of the buildings' states.

    ### Action space
    The action of this environment are for each building
    1. the action (charge or discharge) for the battery (if there is one)
    2. The power delivered to the electric heater (if there is one)
    3. the power delivered to the heat pump for cooling (if there is one)
    4. the power delivered to the heat pump for heating (if there is one)
    Like for the state, the actions of the smartgrid are the concatenation of
    the buildings' actions.
    """

    def __init__(
        self,
        building_path: str,
        weather_path: str,
        nb_days: int,
        initial_day: int = None,
    ):
        """
        Args:
            building_path:
                A string giving the path of the building json file.
            weather_path:
                A string giving the path of the weather csv file.
            nb_days:
                The number of days for the simulation as an int.
            initial_day:
                The first day of the simulation (0 is 01/01).
        """
        # time initialization
        self.hour = 0
        self.day = initial_day if initial_day is not None else 0
        self.initial_day = initial_day
        self.nb_days = nb_days

        # using data loaders
        (
            self.buildings,
            self.observation_spaces,
            self.action_spaces,
            self.n_buildings,
        ) = load_building(building_path)
        # to be defined when defining the buildings
        self.observation_space = self.observation_spaces
        self.action_space = self.action_spaces
        self.last_consumer_consumption = 0
        self.last_consumer_production = 0

        temperatures, solar, _, _ = load_weather_data(weather_path)
        self.solar_energies = solar.reshape(
            (solar.shape[0] // NB_HOURS_PER_DAY, NB_HOURS_PER_DAY)
        )
        self.temperatures = temperatures.reshape(
            (temperatures.shape[0] // NB_HOURS_PER_DAY, NB_HOURS_PER_DAY)
        )

    def get_state_action_space(self) -> (gym.spaces.Box, gym.spaces.Box):
        """Returns the state and action spaces of the environment."""
        return self.observation_spaces, self.action_spaces

    def get_date(self) -> (int, int):
        """
        Returns:
            The current day and the current hour in the simulation.
        """
        return self.day, self.hour

    def get_current_solar_energy(self) -> float:
        """
        Returns:
             The current solar incident energy.
        """
        return self.solar_energies[self.day][self.hour]

    def get_outside_temperature(self) -> float:
        """
        Returns:
            The current outside temperature.
        """
        return self.temperatures[self.day][self.hour]

    def get_daily_outside_temperatures(self) -> list:
        """
        Returns:
             The outside temperatures at each hour for the current day.
        """
        return [self.temperatures[self.day][h] for h in range(NB_HOURS_PER_DAY)]

    def get_daily_solar(self) -> list:
        """
        Returns:
             The solar incident energy at each hour for the current day.
        """
        return [
            self.solar_energies[self.day][h] for h in range(NB_HOURS_PER_DAY)
        ]

    def update_next_time(self) -> None:
        """Add one hour to the current time."""
        self.day += (self.hour + 1) // NB_HOURS_PER_DAY
        self.hour = (self.hour + 1) % NB_HOURS_PER_DAY

    def step(self, action: list) -> (np.array, float, bool, dict):
        """Gym environment step function.

        Args:
            action: Actions given to the consumer.

        Returns:
            states: New state for the producer after executing the actions.
            reward: Reward returned.
            finished: True if the simulation is over.
            info: A dict with info on the simulation.
        """
        electricity_required = 0
        action_id = 0
        building_temperatures = []
        building_consumptions = []
        for building in self.buildings:
            # building component update according to actions

            if building.solar_generator is not None:
                building.solar_generator.set_energy_input(
                    self.solar_energies[self.day][self.hour]
                )

            if building.electricity_storage is not None:
                building.electricity_storage.update_battery(action[action_id])
                action_id += 1

            if building.electric_heater is not None:
                building.electric_heater.set_thermal_energy(action[action_id])
                action_id += 1

            if building.hp_device is not None:
                (
                    energy_cooling,
                    energy_heating,
                ) = building.hp_device.set_thermal_energy(
                    action[action_id],
                    action[action_id + 1],
                    self.temperatures[self.day][self.hour],
                )
                action[action_id], action[action_id + 1] = (
                    energy_cooling,
                    energy_heating,
                )

                action_id += 2
            building.update_temperature(self.temperatures[self.day][self.hour])
            building_temperatures.append(building.get_temperature())

            electricity_required += building.get_energy_consumption(self.hour)
            building_consumptions.append(electricity_required)

        self.update_next_time()

        states = []
        for building in self.buildings:
            # retrieve new state from components
            states.extend(
                (
                    self.temperatures[self.day][self.hour],
                    building.last_temperature,
                )
            )
            if building.solar_generator is not None:
                states.append(self.solar_energies[self.day][self.hour])

            if building.electricity_storage is not None:
                states.append(building.electricity_storage.get_soc())

        building_consumptions = np.array(building_consumptions)
        self.last_consumer_consumption = building_consumptions[
            building_consumptions >= 0.0
        ].sum()
        self.last_consumer_production = -building_consumptions[
            building_consumptions < 0
        ].sum()
        states = np.array(states)
        reward = reward_func_consumer(
            building_consumptions, building_temperatures
        )
        finished = self.day >= self.nb_days

        return states, reward, finished, {}

    def get_state(self):
        """Returns the current state of the environment."""

        states = []
        for building in self.buildings:
            # retrieve new state from components
            states.extend(
                (
                    self.temperatures[self.day][self.hour],
                    building.last_temperature,
                )
            )
            if building.solar_generator is not None:
                states.append(self.solar_energies[self.day][self.hour])

            if building.electricity_storage is not None:
                states.append(building.electricity_storage.get_soc())

        return np.array(states)

    def get_solar_energy(self) -> np.array:
        """
        Returns:
             The incident solar energy for each hour of the year.
        """
        return np.array(self.solar_energies).flatten()

    def get_building_consumption(self) -> list:
        """
        Returns:
             The current electricity consumption of each building.
        """
        return [building.get_last_consumption() for building in self.buildings]

    def get_generated_energy(self) -> float:
        """
        Returns:
             The sum of the generated electricity by all the buildings.
        """
        generated_per_building = [
            building.get_generated_energy()
            for building in self.buildings
            if building.solar_generator is not None
        ]

        generated_per_building = np.array(generated_per_building)

        return np.sum(generated_per_building, axis=0)

    def describe(self) -> None:
        """Logging function for the class."""
        for building in self.buildings:
            building.describe()

    def render(self, mode: str = "human") -> None:
        """Render the environment."""
        print(self.observation_spaces, self.action_spaces)

    def reset(self) -> Optional[np.array]:
        """Gym environment reset function."""
        self.hour = 0
        self.day = self.initial_day
        self.last_consumer_consumption = 0
        for building in self.buildings:
            building.reset()

        return self.get_state()
