import logging

# Agent parameters path
ACTOR_DDPG_PATH = "./models/actor_ddpg_agent.pth"
CRITIC_DDPG_PATH = "./models/critic_ddpg_agent.pth"
DQN_PATH = "./models/branching_dqn_agent.pth"

# Dataset variables
CITIES = ["paris", "la_rochelle", "calais", "nice"]
ID_CITY = 1

BUILDING_PATH = "./data/buildings.json"
TEMPERATURE_PATH = "./data/newyork_temperatures.csv"
SOLAR_PATH = "./data/newyork_solar.csv"
WEATHER_PATH = "./data/" + CITIES[ID_CITY] + "_hourly_weather.csv"

# Consumer side variables
# from https://www.abcclim.net/calcul-bilan-thermique.html
G_ISO = [0.65, 0.75, 0.9, 1.2, 1.8]


# Producer side variables
ENERGY_TYPES = ["fossil", "solar", "nuclear", "wind"]
ENERGY_COLORS = ["black", "red", "yellow", "turquoise"]
POWER_EFFICIENCY = {"fossil": 0.2, "solar": 0.9, "nuclear": 0.7, "wind": 0.9}
MAX_PERC = [0.2, 0.2, 0.6, 0.2]
POSSIBLE_PERCENTAGE = {"fossil": 0.2, "solar": 0.1, "wind": 0.1, "nuclear": 0.6}
ENERGY_PRICES = {
    "fossil": 100 / 1000,
    "solar": 142 / 1000,
    "nuclear": 32 / 1000,
    "wind": 90 / 1000,
}
# electricity price per kWh from https://bit.ly/37mPGyY
MEAN_SOLAR_INCIDENT = 110 / 1000
OPTI_WIND_SPEED = 10.0
MIN_WIND_SPEED = 4.0
NUCLEAR_STARTING_TIME = 2
FOSSIL_STARTING_TIME = 0
ELEC_PRICE = 230 / 1000  # 42€/MWh cf https://bit.ly/3kHVjdX
PRODUCER_MARGIN = 0.0  # as a percentage

# TIME PARAMETERS FOR THE PROBLEM
INITIAL_DAY = 0
NB_DAYS = 30

# AGENT PARAMETERS
T_MIN, T_MAX = 19, 21
INF_REWARD = 10
NB_EPISODE_ACTOR_CRITIC = 20
NB_EPISODE_DISCRETE_DQN = 1000
NB_BIN = 3

# GENERAL CONSTANTS
NB_HOURS_PER_DAY = 24
CELSIUS_TO_KELVIN = 273
NB_SEC_PER_HOUR = 3600
C_VOL_AIR = 1256

# logging variables
logging.basicConfig(
    filename="logging_file.log", encoding="utf-8", level=logging.INFO
)
