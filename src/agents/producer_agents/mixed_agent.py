from src.config import ENERGY_TYPES, POSSIBLE_PERCENTAGE
from src.smartgrid.smartgrid_producer import SmartGridProducer


class MixedAgent:
    """Another bruteforce agent which simulate a producer.

    This agent tries to achieve the energy mix from the list
    POSSIBLE_PERCENTAGE which is defined in config.py. If the production is
    less than the demand, the agent compensates with fossil energy.
    Next agents might usr RL to simulate a producer.
    """

    def get_actions(self, smartgrid: SmartGridProducer) -> (list, list):
        """Computes the action (the energy mix) for the producer.

        Returns:
            Two lists are returned. The first one is the input energy mix for
        the producer and the second one is the output energy mix given to the
        consumer.
        """
        actions, generated, energy_generated = [], [], 0
        for energy_type in ENERGY_TYPES:
            # for each energy type, the agent aims at generating a specified
            # percentage from POSSIBLE_PERCENTAGE
            factor = smartgrid.get_generation(energy_type, 1)
            if (
                smartgrid.consumer_consumption < 0
                or factor == 0
                and energy_type in ["solar", "wind"]
            ):
                energy = 0
            elif factor != 0:
                energy = (
                    smartgrid.consumer_consumption
                    * POSSIBLE_PERCENTAGE[energy_type]
                    / factor
                )
            else:
                energy = 1

            energy_generated += smartgrid.get_generation(energy_type, energy)
            generated.append(smartgrid.get_generation(energy_type, energy))
            actions.append(energy)

        # if the consumers need more energy, fossil energy is generated
        if energy_generated < smartgrid.consumer_consumption:
            factor = smartgrid.get_generation("fossil", 1)
            energy = (
                smartgrid.consumer_consumption - energy_generated
            ) / factor
            actions[ENERGY_TYPES.index("fossil")] += energy
            generated[ENERGY_TYPES.index("fossil")] += smartgrid.get_generation(
                "fossil", energy
            )

        return actions, generated
