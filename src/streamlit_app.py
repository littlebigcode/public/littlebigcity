import matplotlib.pyplot as plt
import numpy as np
import plotly
import plotly.graph_objects as go
import streamlit as st
from PIL import Image
from plotly.subplots import make_subplots

from src.agents.consumer_agents.bruteforce_agent import BruteforceAgent
from src.agents.producer_agents.mixed_agent import MixedAgent
from src.config import (
    BUILDING_PATH,
    CITIES,
    ENERGY_COLORS,
    ENERGY_TYPES,
    ID_CITY,
    INITIAL_DAY,
    NB_DAYS,
    NB_HOURS_PER_DAY,
    T_MAX,
    T_MIN,
    WEATHER_PATH,
)
from src.smartgrid.smartgrid_consumer import SmartGridConsumer
from src.smartgrid.smartgrid_producer import SmartGridProducer


def put_color(text: str, text_color: str) -> str:
    """Put s in the color c in html.

    Args:
        text: String.
        color: Color (as a css color).

    Returs:
        The html code to write the string text in the specified color.
    """
    return f'<p style="color:{text_color}">' + text + "</p>"


def get_date(hour_to_convert: int) -> (int, int):
    """Compute the date.

    Args:
        hour_to_convert: Number of hours since the beginning of the simulation.

    Returns:
        The current month, day and hour.
    """
    day_per_month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    month, day = 1, 1
    while day_per_month[month - 1] * NB_HOURS_PER_DAY <= hour_to_convert:
        hour_to_convert -= day_per_month[month - 1] * NB_HOURS_PER_DAY
        month += 1
    day += hour_to_convert // NB_HOURS_PER_DAY

    return month, day, hour_to_convert % NB_HOURS_PER_DAY


# load images
if "house_image" not in st.session_state:
    st.session_state.house_image = Image.open("../img/home.png")
if "lbc_logo" not in st.session_state:
    st.session_state.lbc_logo = Image.open("../img/logo_LBCity.png")
if "schema_image" not in st.session_state:
    st.session_state.schema_image = Image.open(
        "../img/littlebigcity_schema.png"
    )

# loading smartgrids
if "smartgrid_consumer" not in st.session_state:
    st.session_state.smartgrid_consumer = SmartGridConsumer(
        BUILDING_PATH, WEATHER_PATH, NB_DAYS, INITIAL_DAY
    )
if "smartgrid_producer" not in st.session_state:
    st.session_state.smartgrid_producer = SmartGridProducer(
        WEATHER_PATH, INITIAL_DAY
    )

if "hour_button" not in st.session_state:
    st.session_state.hour_button = False
if "day_button" not in st.session_state:
    st.session_state.day_button = False
if "reset_button" not in st.session_state:
    st.session_state.reset_button = False
(
    observation_space,
    action_space,
) = st.session_state.smartgrid_consumer.get_state_action_space()
temps = [[] for i in range(st.session_state.smartgrid_consumer.n_buildings)]

# loading agents
if "consumer_agent" not in st.session_state:
    st.session_state.consumer_agent = BruteforceAgent()
if "producer_agent" not in st.session_state:
    st.session_state.producer_agent = MixedAgent()

# creating plot variables
if "temperatures" not in st.session_state:
    st.session_state.temperatures = []
if "ext_temperatures" not in st.session_state:
    st.session_state.ext_temperatures = []
if "productions" not in st.session_state:
    st.session_state.productions = []
if "batteries" not in st.session_state:
    st.session_state.batteries = []
if "consumers" not in st.session_state:
    st.session_state.consumers = []
if "prod_energy_prices" not in st.session_state:
    st.session_state.prod_energy_prices = []
if "prod_revenues" not in st.session_state:
    st.session_state.prod_revenues = []
if "elec_cost" not in st.session_state:
    st.session_state.elec_cost = []
if "cons_energy_prices" not in st.session_state:
    st.session_state.cons_energy_prices = []
if "solar_production" not in st.session_state:
    st.session_state.solar_production = []


# Page information
tabs = ["simulator", "graphs"]

st.set_page_config(
    layout="centered", page_icon="🏙️", page_title="LittleBigCity demo"
)


def advance(nb_of_days: int) -> None:
    """Advance the simulation by nb_of_days.

    Args:
        nb_of_days: Number of days to advance the simulation.

    """
    if nb_of_days == 0:
        st.session_state.smartgrid_consumer.reset()
        st.session_state.smartgrid_producer.reset()

        st.session_state.temperatures = []
        st.session_state.ext_temperatures = []
        st.session_state.productions = []
        st.session_state.batteries = []
        st.session_state.consumers = []
        st.session_state.prod_energy_prices = []
        st.session_state.elec_cost = []
        st.session_state.cons_energy_prices = []
        st.session_state.solar_production = []
        st.session_state.prod_revenues = []

    # updating smartgrid state nb_rep times

    for _ in range(nb_of_days):
        actions, _ = st.session_state.consumer_agent.get_actions(
            st.session_state.smartgrid_consumer
        )

        # step for the consumer and the producer
        (
            _,
            _,
            _,
            _,
        ) = st.session_state.smartgrid_consumer.step(np.array(actions))

        st.session_state.smartgrid_producer.consumer_consumption = max(
            0.0,
            st.session_state.smartgrid_consumer.last_consumer_consumption
            - st.session_state.smartgrid_consumer.last_consumer_production,
        )
        st.session_state.smartgrid_producer.consumer_production = (
            st.session_state.smartgrid_consumer.last_consumer_production
        )

        actions_prod, generated = st.session_state.producer_agent.get_actions(
            st.session_state.smartgrid_producer
        )
        (
            _,
            _,
            _,
            _,
        ) = st.session_state.smartgrid_producer.step(actions_prod)

        # updating graph variables
        st.session_state.temperatures.append(
            [
                building.get_temperature()
                for building in st.session_state.smartgrid_consumer.buildings
            ]
        )
        st.session_state.ext_temperatures.append(
            st.session_state.smartgrid_consumer.get_outside_temperature()
        )
        st.session_state.productions.append(generated)
        st.session_state.batteries.append(
            [
                building.get_last_battery()
                for building in st.session_state.smartgrid_consumer.buildings
            ]
        )
        st.session_state.consumers.append(
            [
                building.get_last_consumption()
                for building in st.session_state.smartgrid_consumer.buildings
            ]
        )
        st.session_state.prod_energy_prices.append(
            st.session_state.smartgrid_producer.get_last_production_price()
        )
        st.session_state.prod_revenues.append(
            st.session_state.smartgrid_producer.get_last_selling_price()
        )

        st.session_state.elec_cost.append(
            st.session_state.smartgrid_producer.last_unit_price
        )
        consumer_consumptions = np.array(
            [
                building.get_last_consumption()
                for building in st.session_state.smartgrid_consumer.buildings
            ]
        )
        if st.session_state.smartgrid_producer.consumer_consumption == 0.0:
            # matching consumer production and consumer consumption
            # if the production is higher than the consumption
            consumer_consumptions[consumer_consumptions < 0.0] = (
                consumer_consumptions[consumer_consumptions < 0.0]
                * consumer_consumptions[consumer_consumptions > 0.0].sum()
                / consumer_consumptions[consumer_consumptions < 0.0].sum()
            )

        st.session_state.cons_energy_prices.append(
            list(
                st.session_state.smartgrid_producer.last_unit_price
                * consumer_consumptions
            )
        )
        st.session_state.solar_production.append(
            [
                building.get_last_generation()
                for building in st.session_state.smartgrid_consumer.buildings
            ]
        )
        st.session_state.generated = generated


# sidebar
with st.sidebar:
    date_day, date_hour = st.session_state.smartgrid_consumer.get_date()
    st.image(st.session_state.lbc_logo, width=200)
    st.text("Smartgrid simulator step-by-step")

    cur_month, cur_day, cur_hour = get_date(
        NB_HOURS_PER_DAY * date_day + date_hour
    )

    viz_option = st.selectbox(
        "Visualisation", ("Home", "Current state", "Graphs")
    )

    if date_day - INITIAL_DAY >= NB_DAYS:
        st.text("Simulation is over, please reset")
        if st.button("reset"):
            st.session_state.smartgrid_consumer.reset()
            st.session_state.smartgrid_producer.reset()

            st.session_state.temperatures = []
            st.session_state.ext_temperatures = []
            st.session_state.productions = []
            st.session_state.batteries = []
            st.session_state.consumers = []
            st.session_state.prod_energy_prices = []
            st.session_state.elec_cost = []
            st.session_state.cons_energy_prices = []
            st.session_state.solar_production = []
            st.session_state.prod_revenues = []

    else:

        st.text(f"Date is {cur_day}/{cur_month} at {cur_hour}h")
        st.text(f"City is {CITIES[ID_CITY]}")
        st.text(f"Day {(date_day - INITIAL_DAY)} / {NB_DAYS}")
        bar_day = st.progress((date_day - INITIAL_DAY) / NB_DAYS)
        st.text(f"Hour {date_hour} / {NB_HOURS_PER_DAY}")
        bar_hour = st.progress(date_hour / NB_HOURS_PER_DAY)
        st.session_state.hour_button = st.button(
            "+1 hour", on_click=advance, args=(1,)
        )
        st.session_state.day_button = st.button(
            "+1 day", on_click=advance, args=(24,)
        )
        st.session_state.reset_button = st.button(
            "reset", on_click=advance, args=(0,)
        )

# plotting graphs with plotly
if viz_option == "Home":
    st.text("This is a visualization tool for smartgrid simulation")
    st.image(st.session_state.schema_image)
    st.markdown(
        "You can access our Gitlab "
        "[here](https://gitlab.com/littlebigcode/projets/littlebigcity)"
    )

elif viz_option == "Graphs":

    if len(st.session_state.productions) >= 1:
        if len(st.session_state.productions) > 1:
            start_time, end_time = st.slider(
                "Time interval (in hour)",
                min_value=0,
                max_value=len(st.session_state.productions) - 1,
                value=(0, len(st.session_state.productions) - 1),
            )
        else:
            start_time, end_time = 0, 0

        fig_cons = make_subplots(
            rows=2,
            cols=2,
            subplot_titles=(
                "Temperature (°C)",
                "Electricity consumption (kWh)",
                "Battery percentage (%)",
                "Electricity production (kWh)",
            ),
        )
        fig_elec = make_subplots(
            rows=2,
            cols=2,
            subplot_titles=(
                "Electricity cost per kWh ($/kWh)",
                "Producer cumulative revenue ($)",
                "Consumer hourly electricity price($)",
                "Consumer cumulative electricity price($)",
            ),
        )

        fig_mix = make_subplots(
            rows=1, cols=1, subplot_titles=("Production mix (kWh)",)
        )
        x = list(range(start_time, end_time + 1))

        cum_cons_energy_prices = np.cumsum(
            np.array(st.session_state.cons_energy_prices)[
                start_time : end_time + 1, :
            ],
            axis=0,
        )

        for i in range(st.session_state.smartgrid_consumer.n_buildings):
            fig_cons.add_trace(
                go.Scatter(
                    name=f"Building {i}",
                    x=x,
                    y=[tmp[i] for tmp in st.session_state.temperatures][
                        start_time : end_time + 1
                    ],
                    legendgroup=str(i),
                    marker={"color": plotly.colors.qualitative.Plotly[i]},
                ),
                row=1,
                col=1,
            )
            fig_cons.add_trace(
                go.Scatter(
                    name=f"Building {i}",
                    x=x,
                    y=[tmp[i] for tmp in st.session_state.consumers][
                        start_time : end_time + 1
                    ],
                    showlegend=False,
                    legendgroup=str(i),
                    marker={"color": plotly.colors.qualitative.Plotly[i]},
                ),
                row=1,
                col=2,
            )

            fig_cons.add_trace(
                go.Scatter(
                    name=f"Building {i}",
                    x=x,
                    y=[tmp[i] for tmp in st.session_state.batteries][
                        start_time : end_time + 1
                    ],
                    showlegend=False,
                    legendgroup=str(i),
                    marker={"color": plotly.colors.qualitative.Plotly[i]},
                ),
                row=2,
                col=1,
            )
            fig_cons.add_trace(
                go.Scatter(
                    name=f"Building {i}",
                    x=x,
                    y=[tmp[i] for tmp in st.session_state.solar_production][
                        start_time : end_time + 1
                    ],
                    showlegend=False,
                    legendgroup=str(i),
                    marker={"color": plotly.colors.qualitative.Plotly[i]},
                ),
                row=2,
                col=2,
            )
            fig_elec.add_trace(
                go.Scatter(
                    name=f"Building {i}",
                    x=x,
                    y=[tmp[i] for tmp in st.session_state.cons_energy_prices][
                        start_time : end_time + 1
                    ],
                    legendgroup=str(i),
                    marker={"color": plotly.colors.qualitative.Plotly[i]},
                ),
                row=2,
                col=1,
            )
            fig_elec.add_trace(
                go.Scatter(
                    name=f"Building {i}",
                    x=x,
                    y=cum_cons_energy_prices[:, i],
                    showlegend=False,
                    legendgroup=str(i),
                    marker={"color": plotly.colors.qualitative.Plotly[i]},
                ),
                row=2,
                col=2,
            )

        fig_cons.add_trace(
            go.Scatter(
                name="Exterior",
                x=x,
                y=st.session_state.ext_temperatures[start_time : end_time + 1],
            ),
            col=1,
            row=1,
        )

        cum_energy_prices = np.cumsum(
            st.session_state.prod_energy_prices[start_time : end_time + 1]
        )
        fig_elec.add_trace(
            go.Scatter(name="Cumulative cost", x=x, y=cum_energy_prices),
            row=1,
            col=2,
        )
        fig_elec.add_trace(
            go.Scatter(
                name="Cost per kWh",
                x=x,
                y=st.session_state.elec_cost[start_time : end_time + 1],
            ),
            row=1,
            col=1,
        )

        prods = np.array(st.session_state.productions)
        for i in range(1, len(ENERGY_TYPES)):
            prods[:, i] += prods[:, i - 1]

        fig_mix.add_trace(
            go.Scatter(
                name=ENERGY_TYPES[0],
                x=x,
                y=prods[start_time : end_time + 1, 0],
                fill="tozeroy",
                marker={"color": ENERGY_COLORS[0]},
            ),
            row=1,
            col=1,
        )

        for i in range(1, len(ENERGY_TYPES)):
            fig_mix.add_trace(
                go.Scatter(
                    name=ENERGY_TYPES[i],
                    x=x,
                    y=prods[start_time : end_time + 1, i],
                    fill="tonexty",
                    marker={"color": ENERGY_COLORS[i]},
                ),
                row=1,
                col=1,
            )

        st.plotly_chart(fig_mix)
        st.plotly_chart(fig_cons)
        st.plotly_chart(fig_elec)

    else:
        st.text("Too soon")

# visualizing the smartgrid
elif viz_option == "Current state":
    if len(st.session_state.productions) >= 1:
        cols = st.columns(3)

        cols[0].text("Producer total revenu")
        cols[0].text(np.sum(np.array(st.session_state.prod_revenues)))

        cols[1].text("Producer total cost ($)")
        cols[1].text(np.sum(np.array(st.session_state.prod_energy_prices)))

        cols[2].text("Consumer total cost ($)")
        cols[2].text(np.sum(np.array(st.session_state.cons_energy_prices)))

        fig, ax = plt.subplots()
        st.markdown(
            '<h2 style="text-align: center"> Production energy mix</h2>',
            unsafe_allow_html=True,
        )
        if "generated" in st.session_state:
            if np.max(st.session_state.generated) == 0.0:
                st.text("No generation")
            else:

                fig = go.Figure(
                    go.Pie(
                        labels=ENERGY_TYPES,
                        values=st.session_state.generated,
                        marker=dict(colors=ENERGY_COLORS),
                    )
                )

                st.plotly_chart(fig, height=1600)

        st.markdown(
            '<h2 style="text-align: center"> Consumer state</h2>',
            unsafe_allow_html=True,
        )
        cols = st.columns(st.session_state.smartgrid_consumer.n_buildings)
        for id_building in range(
            st.session_state.smartgrid_consumer.n_buildings
        ):
            building = st.session_state.smartgrid_consumer.buildings[
                id_building
            ]
            cols[id_building].image(st.session_state.house_image, width=50)
            cols[id_building].text(f"Building {id_building}:")

            building_expense = np.array(st.session_state.cons_energy_prices)[
                :, id_building
            ]
            cols[id_building].text(
                "Total revenue: " f"{building_expense.sum():.2f} $"
            )
            cols[id_building].text(
                f"Spent: {building_expense[building_expense > 0].sum():.2f} $"
            )
            cols[id_building].text(
                "Earnt: "
                f"{np.abs(building_expense[building_expense < 0]).sum():.2f} $"
            )

            if building.last_outside_temp is not None:
                p = f"Outside: {building.last_outside_temp:.2f} °C"
                cols[id_building].text(p)

            t = building.get_temperature()
            if T_MIN <= t <= T_MAX:
                COLOR = "Green"
            elif min(abs(t - T_MIN), abs(t - T_MAX)) <= 0.5:
                COLOR = "Orange"
            else:
                COLOR = "Red"

            p = f"Inside: {building.get_temperature():.2f} °C"
            cols[id_building].markdown(
                put_color(p, COLOR), unsafe_allow_html=True
            )

            if building.electricity_storage is not None:
                soc = int(100 * building.electricity_storage.get_soc())

                if 50.0 <= soc <= 100.0:
                    COLOR = "Green"
                elif 25.0 <= soc <= 50.0:
                    COLOR = "Orange"
                else:
                    COLOR = "Red"

                p = (
                    "Battery:"
                    f"{int(100 * building.electricity_storage.get_soc())}%"
                )
                cols[id_building].markdown(
                    put_color(p, COLOR), unsafe_allow_html=True
                )

                battery_consumption = (
                    building.electricity_storage.get_energy_consumption()
                )

                if battery_consumption > 0:
                    p = f"Battery consumption: -{battery_consumption} kWh"
                    COLOR = "Red"
                elif battery_consumption < 0:
                    p = f"Battery consumption: +{-battery_consumption} kWh"
                    COLOR = "Green"
                cols[id_building].markdown(
                    put_color(p, COLOR), unsafe_allow_html=True
                )
            else:
                cols[id_building].markdown(
                    put_color("No battery", "Grey"), unsafe_allow_html=True
                )
                cols[id_building].markdown(
                    put_color("No battery", "Grey"), unsafe_allow_html=True
                )

            if building.solar_generator is not None:
                p = (
                    f"Solar Panel generation: +"
                    f"{-building.solar_generator.get_energy_consumption()} kWh"
                )
                cols[id_building].markdown(
                    put_color(p, "Green"), unsafe_allow_html=True
                )
            else:
                cols[id_building].markdown(
                    put_color("No solar panel", "Grey"), unsafe_allow_html=True
                )

            p = (
                f"Appliances consumption: -"
                f"{building.last_appliances_consumption} kWh"
            )
            cols[id_building].markdown(
                put_color(p, "Red"), unsafe_allow_html=True
            )

            if building.electric_heater is not None:
                p = (
                    f"Electric heater consumption: -"
                    f"{building.electric_heater.get_energy_consumption()} kWh"
                )
                cols[id_building].markdown(
                    put_color(p, "Red"), unsafe_allow_html=True
                )
            else:
                cols[id_building].markdown(
                    put_color("No electric heater", "Grey"),
                    unsafe_allow_html=True,
                )
            if building.hp_device is not None:
                p = (
                    f"Heat pump consumption: -"
                    f"{building.hp_device.get_energy_consumption()} kWh"
                )
                cols[id_building].markdown(
                    put_color(p, "Red"), unsafe_allow_html=True
                )
            else:
                cols[id_building].markdown(
                    put_color("No heat pump", "Grey"), unsafe_allow_html=True
                )
    else:
        st.text("Too soon")
