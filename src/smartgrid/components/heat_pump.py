from src.config import CELSIUS_TO_KELVIN, logging


class HeatPump:
    """Class for the heat pump of the building.

    This component can be used to cool and to heat the building.

    Args:
        efficiency_coeff: Efficiency of the heatpump (as a %).
        max_input_energy: Maximum electric energy that can be given in kWh.
        t_hot: Temperature of the hot thermostat for heating.
        t_cold: Temperature of the cold thermostat for cooling.

    Attributes:
        efficiency_coeff: Efficiency of the heatpump (as a %).
        max_input_energy: Maximum electric energy that can be given in kWh.
        t_hot: Temperature of the hot thermostat for heating.
        t_cold: Temperature of the cold thermostat for cooling.
        released_energy: Current thermal energy released in kWh.
        energy_history: History of the released_energy at each timestep.
        last_consumption: Last consumption of the heat pump in kWh.
        last_cooling_consumption: Last consumption for cooling in kWh.
        last_heating_consumption: Last consumption for heating in kWh.
    """

    def __init__(
        self,
        max_consumption: float = None,
        efficiency_coeff: float = None,
        t_hot: float = None,
        t_cold: float = None,
    ):

        # initialize attributes
        self.max_consumption = max_consumption
        self.efficiency_coeff = efficiency_coeff

        self.t_hot = t_hot
        self.t_cold = t_cold

        self.last_consumption = 0
        self.last_heating_consumption = 0
        self.last_cooling_consumption = 0
        self.released_energy = None
        self.energy_history = []

    def set_thermal_energy(
        self, energy_cooling: float, energy_heating: float, t_outside: float
    ) -> None:
        """Update the heatpump given the input energy.

        Args:
            energy_cooling: Input energy for cooling in kWh.
            energy_heating: Input energy for heating in kWh.
            t_outside: Outside temperature.
        """
        if t_outside >= self.t_hot - 0.5:
            energy_heating = 0
            cop_heating = 0
        else:
            cop_heating = (
                self.efficiency_coeff
                * (self.t_hot + CELSIUS_TO_KELVIN)
                / (self.t_hot - t_outside)
            )

        if t_outside <= self.t_cold + 0.5:
            energy_cooling = 0
            cop_cooling = 0
        else:
            cop_cooling = (
                self.efficiency_coeff
                * (self.t_cold + CELSIUS_TO_KELVIN)
                / (t_outside - self.t_cold)
            )

        if self.max_consumption is None:
            self.released_energy = (
                cop_heating * energy_heating - cop_cooling * energy_cooling
            )
            self.last_consumption = energy_cooling + energy_heating
        else:
            self.released_energy = cop_heating * min(
                energy_heating, self.max_consumption
            ) - cop_cooling * min(energy_cooling, self.max_consumption)
            self.last_consumption = min(
                energy_heating, self.max_consumption
            ) + min(energy_cooling, self.max_consumption)

        self.energy_history.append(self.released_energy)
        self.last_cooling_consumption = energy_cooling
        self.last_heating_consumption = energy_heating
        return energy_cooling, energy_heating

    def get_thermal_energy(self) -> float:
        """
        Returns:
            The thermal energy released by the heatpump (it can be negative).
        """
        return self.released_energy

    def get_energy_consumption(self) -> float:
        """
        Returns:
            The electricity consumption of the heatpump.
        """
        return self.last_consumption

    def describe(self) -> None:
        """Logging function for the heatpump."""
        logging.info(
            "Heatpump:\n"
            f"Energy released= {self.released_energy}kWh\n"
            f"Consumption= {self.last_consumption}kWh\n"
        )

    def reset(self) -> None:
        """Reset function for the heatpump."""
        self.released_energy = None
        self.energy_history = []
