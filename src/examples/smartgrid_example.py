import numpy as np

from src.agents.consumer_agents.bruteforce_agent import BruteforceAgent
from src.agents.producer_agents.mixed_agent import MixedAgent
from src.config import (
    BUILDING_PATH,
    INITIAL_DAY,
    NB_DAYS,
    NB_HOURS_PER_DAY,
    WEATHER_PATH,
)
from src.smartgrid.smartgrid_consumer import SmartGridConsumer
from src.smartgrid.smartgrid_producer import SmartGridProducer


def main():
    """Simulation of the smartgrid on NB_DAYS days.

    Both the consumer and the producer act in this simulation.
    The consumer is acting according to the bruteforce agent and the producer
    uses the mixed agent actions.
    """
    # creating the consumer side smartgrid
    smartgrid_consumer = SmartGridConsumer(
        BUILDING_PATH, WEATHER_PATH, NB_DAYS, INITIAL_DAY
    )
    smartgrid_producer = SmartGridProducer(WEATHER_PATH, NB_DAYS)

    consumed = []

    # loading agent
    consumer_agent = BruteforceAgent()
    producer_agent = MixedAgent()

    for _ in range(NB_DAYS):
        for _ in range(NB_HOURS_PER_DAY):

            # getting consumer agent action at each timestep
            actions_cons = consumer_agent.get_action([], smartgrid_consumer)

            # giving the action to the smartgrid consumer side
            _, _, _, _ = smartgrid_consumer.step(np.array(actions_cons))
            consumed.append(smartgrid_consumer.last_consumer_consumption)

            # getting the production agent action
            smartgrid_producer.consumer_consumption = (
                smartgrid_consumer.last_consumer_consumption
            )
            actions_prod, _ = producer_agent.get_actions(smartgrid_producer)

            # giving the action to the smartgrid production side
            _, _, _, _ = smartgrid_producer.step(actions_prod)


if __name__ == "__main__":
    main()
