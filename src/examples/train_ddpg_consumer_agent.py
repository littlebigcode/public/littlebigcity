import itertools

import numpy as np
import stqdm

from src.agents.consumer_agents.a2c_agent import Agent
from src.config import (
    BUILDING_PATH,
    INITIAL_DAY,
    NB_DAYS,
    NB_HOURS_PER_DAY,
    WEATHER_PATH,
)
from src.smartgrid.smartgrid_consumer import SmartGridConsumer


def train_actor_critic(num_episodes=100):
    """Train a RL ActorCritic Baseline agent.

    Returns:
        The trained agent."""

    # creating the consumer side smartgrid

    smartgrid = SmartGridConsumer(
        BUILDING_PATH, WEATHER_PATH, NB_DAYS, INITIAL_DAY
    )
    a2c_agent = Agent(smartgrid)
    for _ in stqdm.stqdm(range(num_episodes), desc="ActorCritic training"):
        state = smartgrid.reset()
        nb_iteration = 0
        for _, _ in itertools.product(range(NB_DAYS), range(NB_HOURS_PER_DAY)):
            actions = a2c_agent.get_noisy_exploration_action(state)
            # Take actions in env and look the results
            next_state, reward, done, _ = smartgrid.step(actions)

            # Add experience to replay memory
            a2c_agent.replay.add_exp(
                state, actions, np.sum(reward), next_state, done
            )
            # Agent optimization
            a2c_agent.optimize()
            state = next_state
            nb_iteration += 1
            if done:
                break

    a2c_agent.save()
    return a2c_agent


if __name__ == "__main__":
    train_actor_critic()
