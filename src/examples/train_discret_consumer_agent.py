import itertools
import logging

import numpy as np
import stqdm

from src.agents.consumer_agents.discret_dqn_agent import (
    BranchingDQNAgent,
    DiscretizedWrapper,
)
from src.config import (
    BUILDING_PATH,
    INITIAL_DAY,
    NB_BIN,
    NB_DAYS,
    NB_HOURS_PER_DAY,
    T_MAX,
    T_MIN,
    WEATHER_PATH,
)
from src.smartgrid.smartgrid_consumer import SmartGridConsumer

logging.basicConfig(
    filename="example.log",
    encoding="utf-8",
    level=logging.CRITICAL,
)


def train_discretized(num_episodes=100):
    """Train a RL ActorCritic Baseline agent.

    Returns:
        The trained agent."""

    # creating the consumer side smartgrid

    smartgrid = DiscretizedWrapper(
        SmartGridConsumer(BUILDING_PATH, WEATHER_PATH, NB_DAYS, INITIAL_DAY),
        NB_BIN,
    )
    agent = BranchingDQNAgent(
        smartgrid,
        smartgrid.get_discretized_array(),
        NB_BIN,
        epsilon_decay=1e-5,
    )

    rewards = []
    for id_episode in stqdm.stqdm(
        range(num_episodes), desc="Discretize DQN training"
    ):
        state = smartgrid.reset()
        nb_iteration = 0
        total_reward = 0
        nb_temp = 0
        for _, _ in itertools.product(range(NB_DAYS), range(NB_HOURS_PER_DAY)):
            actions = agent.get_epsilon_action(state)
            # Take actions in env and look the results
            next_state, reward, done, _ = smartgrid.step(
                actions.astype(np.int32)
            )
            total_reward += np.sum(reward)

            if (
                smartgrid.buildings[0].last_temperature > T_MAX
                or smartgrid.buildings[0].last_temperature < T_MIN
            ):
                nb_temp += 1
            # Add experience to replay memory
            agent.memory.add_exp(
                state, actions, np.sum(reward), next_state, done
            )
            # Agent optimization
            agent.update_policy()
            state = next_state
            nb_iteration += 1
            if done:
                break
        rewards.append(total_reward)

        print(
            f"Episode: {id_episode} out of {num_episodes},"
            f" Reward: {total_reward}, Temperature: {nb_temp}"
        )

    agent.save()

    return agent


if __name__ == "__main__":
    train_discretized()
