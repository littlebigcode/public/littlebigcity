import numpy as np

from src.agents.consumer_agents.bruteforce_agent import BruteforceAgent
from src.config import (
    BUILDING_PATH,
    INITIAL_DAY,
    NB_DAYS,
    NB_HOURS_PER_DAY,
    WEATHER_PATH,
)
from src.smartgrid.smartgrid_consumer import SmartGridConsumer


def main():
    """Example script to simulate the smartgrid on the consumer side without
    a producer.
    """
    # creating the consumer side smartgrid
    smartgrid = SmartGridConsumer(
        BUILDING_PATH, WEATHER_PATH, NB_DAYS, INITIAL_DAY
    )

    consumed = []

    # loading agent
    bruteforce_agent = BruteforceAgent()

    for _ in range(NB_DAYS):
        for _ in range(NB_HOURS_PER_DAY):
            # getting agent action at each timestep
            actions = bruteforce_agent.get_action([], smartgrid)

            # giving the action to the smartgrid
            _, _, _, _ = smartgrid.step(np.array(actions))
            consumed.append(smartgrid.last_consumer_consumption)


if __name__ == "__main__":
    main()
