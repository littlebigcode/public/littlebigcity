import gym
import numpy as np
import torch
import torch.nn.functional as F
from torch import nn
from torch.autograd import Variable

from src.config import DQN_PATH
from src.smartgrid.smartgrid_consumer import SmartGridConsumer


class BranchingQNetwork(nn.Module):
    """Neural Network for the Q-function.

    Args:
        state_dim: Dimension of the state.
        action_dim: Dimension of the action.
        nb_action: Number of possible action for each dimension.

    Attributes:
        model: The neural network model.
        value_net: The neural network model for the value function.
        advantage_net: The neural network model for the advantage function.
    """

    def __init__(self, state_dim: int, action_dim: int, nb_action: int):
        super().__init__()

        self.action_dim = action_dim
        self.nb_action = nb_action
        self.model = nn.Sequential(
            nn.Linear(state_dim, 128), nn.ReLU(), nn.Linear(128, 128), nn.ReLU()
        )
        self.value_net = nn.Linear(128, 1)
        self.advantage_net = nn.ModuleList(
            [nn.Linear(128, nb_action) for i in range(action_dim)]
        )

    def forward(self, state: np.array) -> torch.Tensor:
        """Forward pass of the neural network.

        Args:
            state: Current state of the environment.
        """
        out = self.model(state)
        value = self.value_net(out)
        advantages = torch.stack(
            [net(out) for net in self.advantage_net], dim=1
        )

        q_values = (
            value.unsqueeze(-1) + advantages - advantages.mean(2, keepdim=True)
        )

        return q_values


class BranchingDQNAgent:
    """Branching Q Network Agent.

    Args:
        env: Environment to train on.
        nb_action: Number of possible action for each dimension.
        gamma: Discount factor.
        batch_size: Size of the batch.
        learning_rate: Learning rate of the optimizer.
        epsilon_decay: Decay rate of the epsilon.
        min_epsilon: Minimum value of the epsilon.
        net_update_freq: Frequency of the network update.

    Attributes:
        memory: Replay memory of the agent.
        model: The neural network model.
        target: The target network.
        optimizer: The optimizer of the network.
        epsilon: The epsilon value of the agent.
        iter: The number of update iterations.
    """

    def __init__(
        self,
        env: gym.Env,
        discrete_to_continuous: np.array,
        nb_action: int,
        gamma: float = 0.99,
        batch_size: int = 128,
        learning_rate: float = 0.0001,
        epsilon_decay: float = 1e-4,
        min_epsilon: float = 0.01,
        net_update_freq: int = 1000,
    ):
        super().__init__()
        state_dim, action_dim = (
            env.observation_space.shape[0],
            env.action_space.shape[0],
        )
        self.memory = ExperienceReplay(state_dim, action_dim)
        self.model = BranchingQNetwork(state_dim, action_dim, nb_action)
        self.target = BranchingQNetwork(state_dim, action_dim, nb_action)
        self.target.load_state_dict(self.model.state_dict())
        self.optimizer = torch.optim.Adam(
            self.model.parameters(), lr=learning_rate
        )
        self.discrete_to_continuous = discrete_to_continuous
        self.learning_rate = learning_rate
        self.gamma = gamma
        self.batch_size = batch_size
        self.epsilon = 1.0
        self.epsilon_decay = epsilon_decay
        self.min_epsilon = min_epsilon
        self.net_update_freq = net_update_freq
        self.iter = 0

        self.env = env

    def get_epsilon_action(self, state: np.array) -> np.array:
        """Get an action with epsilon-greedy policy.

        Args:
            state: Current state of the environment.

        Returns:
            action: Action to take."""
        self.epsilon = max(self.epsilon - self.epsilon_decay, self.min_epsilon)
        if np.random.rand() < self.epsilon:
            return self.env.action_space.sample()
        return self.get_discrete_action(state)

    def get_action(
        self, state: np.array, smartgrid: SmartGridConsumer
    ) -> np.array:
        """Get an action with model.

        Args:
            state: Current state of the environment.

        Returns:
            action: Action to take."""
        discrete_action = self.get_discrete_action(state, smartgrid)
        action = np.array(
            [
                self.discrete_to_continuous[a, index]
                for index, a in np.ndenumerate(discrete_action)
            ]
        ).flatten()
        return action

    def get_discrete_action(
        self, state: np.array, smartgrid: SmartGridConsumer = None
    ) -> np.array:
        """Get an action with model.

        Args:
            state: Current state of the environment.

        Returns:
            Action to perform."""
        out = self.model(
            torch.from_numpy(state.astype(np.float32)).unsqueeze(0)
        ).squeeze(0)
        action = torch.argmax(out, dim=1)
        return action.numpy()

    def update_policy(self):
        """Update the policy of the agent."""
        self.iter += 1

        if self.iter < self.net_update_freq:
            return

        states, actions, rewards, next_states, dones = self.memory.sample(
            min(self.batch_size, self.memory.pointer)
        )

        states = Variable(torch.from_numpy(states.astype(np.float32)))
        actions = Variable(torch.from_numpy(actions.astype(np.int64)))
        rewards = Variable(torch.from_numpy(rewards.astype(np.float32)))
        next_states = Variable(torch.from_numpy(next_states.astype(np.float32)))
        dones = Variable(torch.from_numpy(dones.astype(np.float32)))

        current_q_values = (
            self.model(states).gather(2, actions.unsqueeze(-1)).squeeze(-1)
        )

        with torch.no_grad():
            id_max = torch.argmax(self.model(next_states), dim=2)
            next_q_values = (
                self.target(next_states)
                .gather(2, id_max.unsqueeze(2))
                .squeeze(-1)
            )

        expected_q_values = rewards.unsqueeze(
            -1
        ) + next_q_values * self.gamma * (1 - dones).unsqueeze(-1)
        loss = F.mse_loss(current_q_values, expected_q_values)

        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

        if self.iter % self.net_update_freq == 0:
            self.target.load_state_dict(self.model.state_dict())

    def save(self):
        """Save the agent."""
        torch.save(self.model.state_dict(), DQN_PATH)

    def load(self):
        """Load the agent."""

        self.epsilon = self.min_epsilon
        self.model.load_state_dict(torch.load(DQN_PATH))


class ExperienceReplay:
    """Experience replay buffer.

    Args:
        state_dim: Dimension of the observation space.
        action_dim: Dimension of the action space.
        buffer_size: Size of the buffer.

    Attributes:
        state_mem: Memory of the states.
        action_mem: Memory of the actions.
        reward_mem: Memory of the rewards.
        next_state_mem: Memory of the next states.
        done_mem: Memory of the done flags.
        pointer: Pointer to the current position in the memory.
    """

    def __init__(
        self, state_dim: int, action_dim: int, buffer_size: int = 1000000
    ):
        self.buffer_size = buffer_size
        self.state_dim = state_dim
        self.action_dim = action_dim
        self.state_mem = np.zeros((self.buffer_size, state_dim))
        self.action_mem = np.zeros((self.buffer_size, action_dim))
        self.reward_mem = np.zeros(self.buffer_size)
        self.done_mem = np.zeros(self.buffer_size)
        self.next_state_mem = np.zeros((self.buffer_size, state_dim))
        self.pointer = 0

    def add_exp(
        self,
        state: np.array,
        action: np.array,
        reward: int,
        next_state: np.array,
        done: bool,
    ):
        """Add an experience to the memory.

        Args:
            state: Current state of the environment.
            action: Current action of the environment.
            reward: Reward received from the environment.
            next_state: Next state of the environment.
            done: Done flag of the environment."""
        idx = self.pointer % self.buffer_size
        self.state_mem[idx] = state
        self.action_mem[idx] = action
        self.reward_mem[idx] = reward
        self.next_state_mem[idx] = next_state
        self.done_mem[idx] = done
        self.pointer += 1

    def sample(
        self, batch_size: int = 64
    ) -> (np.array, np.array, np.array, np.array, np.array):
        """Sample a batch of experiences from the memory.

        Args:
            batch_size: Size of the batch.

        Returns:
            States, actions, rewards, next states , dones of the environment.
        """
        max_mem = min(self.pointer, self.buffer_size)
        batch = np.random.choice(max_mem, batch_size, replace=False)
        states = self.state_mem[batch]
        actions = self.action_mem[batch]
        rewards = self.reward_mem[batch]
        next_states = self.next_state_mem[batch]
        dones = self.done_mem[batch]

        return states, actions, rewards, next_states, dones


class DiscretizedWrapper(gym.Wrapper):
    """Gym environment wrapper to discretize the action space.

    This wrapper work with continuous action space and discretize it
    with a given number of bins for each action.

    Args:
        nb_bin: Number of bins for each action.
        env: Environment to wrap.

    Attributes:
        discretized: Mapping for the discretized action space.
        action_space: New MultiDiscrete action_space for the environment.
    """

    def __init__(self, env: gym.Env, nb_bin: int):
        super().__init__(env)
        self.nb_bin = nb_bin
        self.discretized = np.linspace(
            env.action_space.low, env.action_space.high, self.nb_bin
        )
        self.action_space = gym.spaces.MultiDiscrete(
            np.array([self.nb_bin] * self.action_space.shape[0])
        )

    def get_discretized_array(self) -> np.array:
        """Get the discretized array.

        Returns:
            Discretized array.
        """
        return self.discretized

    def step(self, action: np.array) -> np.array:
        """Step the environment."""
        action = np.array(
            [self.discretized[a, index] for index, a in np.ndenumerate(action)]
        ).flatten()
        return super().step(action)
