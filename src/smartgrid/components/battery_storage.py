from src.config import logging


class BatteryStorage:
    """Class for the domestic battery of a building.

    Args:
        initial_capacity: Initial capacity of the battery in kWh.
        irreversible_loss: Battery capacity lost every cycle in kWh.
        charging_speed: Capacity charged or discharged at each timestep in kWh.

    Attributes:
        initial_capacity: Initial capacity of the battery in kWh.
        irreversible_loss: Battery capacity lost every cycle in kWh.
        charging_speed: Capacity charged or discharged at each timestep in kWh.
        state_of_charge: Current charge of the battery in kWh.
        soc_history: Charge of the battery at each timestep so far.
        energy_used: Sum of all the energy used for charging and discharging.
        last_action: Last action (charge or discharge) by the battery.
    """

    def __init__(
        self, initial_capacity=None, irreversible_loss=None, charging_speed=None
    ):

        self.initial_capacity = initial_capacity
        self.irreversible_loss = irreversible_loss
        self.charging_speed = charging_speed

        self.state_of_charge = initial_capacity
        self.soc_history = []
        self.energy_used = 0
        self.last_action = None

    def is_valid_action(self, action: float) -> bool:
        """
        Args:
            action: Action (charge or discharge) given to the battery.

        Returns:
            Whether the action is valid given the charge of the battery.
        """
        return (
            0
            <= self.state_of_charge + action * self.charging_speed
            <= self.initial_capacity
            - self.irreversible_loss
            * self.energy_used
            / (2 * self.initial_capacity)
        )

    def update_battery(self, action: float) -> None:
        """Take action (charge or discharge) and apply it to the battery.

        Args:
            action: Action (charge or discharge) given to the battery.
        """
        if self.is_valid_action(action):
            self.last_action = action
            self.state_of_charge += action * self.charging_speed

        self.soc_history.append(
            self.state_of_charge
            / (
                self.initial_capacity
                - self.irreversible_loss
                * self.energy_used
                / (2 * self.initial_capacity)
            )
        )

    def get_soc(self) -> float:
        """
        Returns:
             The current charge of the battery as a percentage.
        """
        return self.state_of_charge / (
            self.initial_capacity
            - self.irreversible_loss
            * self.energy_used
            / (2 * self.initial_capacity)
        )

    def get_energy_consumption(self, action=None) -> float:
        """
        Args:
            action: Action (charge or discharge) given to the battery.

        Returns:
            The current consumption of the battery.
        """
        if action is not None and self.is_valid_action(action):
            return action * self.charging_speed
        if action is None and self.last_action is not None:
            return self.last_action * self.charging_speed
        return 0

    def describe(self) -> None:
        """Logging function for the battery."""
        logging.info(
            "Battery Storage:\n" f"State of Charge= {100 * self.get_soc()}%\n"
        )
        if self.last_action is None:
            return
        if self.last_action > 0:
            logging.info("Charge\n")
        elif self.last_action < 0:
            logging.info("Discharge\n")

    def reset(self) -> None:
        """Reset function for the battery."""
        self.state_of_charge = self.initial_capacity
        self.soc_history = []
        self.energy_used = 0
        self.last_action = None
